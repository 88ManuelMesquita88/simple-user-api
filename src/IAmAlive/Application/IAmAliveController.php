<?php


namespace App\IAmAlive\Application;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IAmAliveController
{

    public function index( Request $request): Response
    {
        return new Response("I Am Alive");
    }
}