<?php


namespace App\User\Domain\Repository;


use App\User\Domain\Entity\User;

interface UserRepositoryInterface
{

    public function getUserById( int $id ) : User;

    public function getUserByUsername( string $username ) : User;

    public function getUserByEmailOrUsername( string $username_or_email ) : User;

    public function getUserList() : array;

    public function save( User $user );

    public function delete( user $user );

}