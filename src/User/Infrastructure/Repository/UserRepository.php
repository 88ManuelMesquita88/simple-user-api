<?php

namespace App\User\Infrastructure\Repository;

use App\User\Domain\Entity\User;
use App\User\Domain\Repository\UserRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository implements UserRepositoryInterface
{

    public function getUserById(int $id) : User
    {
        /** @var User $user */
        $user = $this->find( $id );

        if(empty($user))
            $user = new User();

        return $user;
    }

    public function getUserByUsername(string $username) : User
    {

        /** @var User $user */
        $user = $this->findby( ['username' => [$username]] );

        if(empty($user))
            $user[0] = new User();


        return $user[0];
    }

    public function getUserByEmailOrUsername( $email_or_username) : User
    {

        /** @var User $user */
        $user =$this->createQueryBuilder('u')
            ->where('u.email LIKE :email')
            ->orWhere('u.username LIKE :username')
            ->setParameter('username', '%'.$email_or_username.'%')
            ->setParameter('email', '%'.$email_or_username.'%')
            ->getQuery()
            ->getOneOrNullResult();

        if(is_null($user))
            $user = new User();

        return $user;

    }

    public function getUserList(): array
    {
        return $this->findAll( );
    }

    public function save(User $user)
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function delete(User $user)
    {
        $this->_em->remove($user);
        $this->_em->flush();
    }



}