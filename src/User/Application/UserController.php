<?php


namespace App\User\Application;

use App\User\Domain\Entity\User;
use App\User\Infrastructure\Repository\UserRepository;
use PHPUnit\Util\Json;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class UserController extends AbstractController
{

    protected $repository;
    private $serializer;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);

    }

    public function getList() : JsonResponse
    {
        $list = $this->repository->getUserList();

        if( !empty($list) ){
            $list = $this->serializer->serialize($list, 'json');

        }else{
            return new JsonResponse(array('stt'=>'NOK', 'code' => '4000', 'msg'=>'No Users'));
        }

        return JsonResponse::fromJsonString($list);
    }

    public function getById(Request $request) : JsonResponse
    {
        $user = $this->repository->getUserById( (int) $request->get('id'));

        if( !empty( $user->getUsername() ) ){
            $user = $this->serializer->serialize($user, 'json');

        }else{
            return new JsonResponse(array('stt'=>'NOK', 'code' => '4000' , 'msg'=>'inexistent user'));
        }

        return JsonResponse::fromJsonString($user);
    }

    public function create( Request $request ) : JsonResponse
    {

        $user = new User();
        $user->setUsername($request->get('username'));
        $user->setEmail($request->get('email'));
        $this->repository->save($user);

        return new JsonResponse(array('stt' => 'OK'));

    }

    public function update( Request $request ) : JsonResponse
    {

        //TODO:: create output Class
        if( empty($request->get('username')) )
            return new JsonResponse(array('stt' => 'NOK', 'code'=>'4001', 'msg'=>'Username is mandatory'));

        $user = $this->repository->getUserByEmailOrUsername($request->get('username'));

        if(empty($user->getId()))
            return new JsonResponse(array('stt' => 'NOK', 'code'=>'4002', 'msg'=>'User not found.'));

        $user->setEmail($request->get('email'));
        $this->repository->save($user);

        return new JsonResponse(array('stt' => 'OK'));

    }

    public function delete( Request $request ) : JsonResponse
    {
        //TODO:: create validate to ENTITY
        if( empty($request->get('username')) )
            return new JsonResponse(array('stt' => 'NOK', 'code'=>'4005', 'msg'=>'Username is mandatory'));

        $user = $this->repository->getUserByEmailOrUsername($request->get('username'));

        if(empty($user->getId()))
            return new JsonResponse(array('stt' => 'NOK', 'code'=>'4006', 'msg'=>'User not found.'));


        $this->repository->delete($user);
        return new JsonResponse(array('stt' => 'OK'));


    }


}