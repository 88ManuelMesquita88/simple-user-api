# README #

Symfony 5 Api to get/update/delete 1 entity


### INSTALL ###

1. Git Clone the project
2. Composer update
3. Create your Database
   ~~~~
   "CREATE DATABASE `Api-User`;"
   ~~~~
4. Migrations
   ~~~~
    1. bin/console doctrine:migrations:status
   ~~~~
   ~~~~
    2. bin/console doctrine:migrations:diff  //check for migrations
   ~~~~
   ~~~~
    3. bin/console doctrine:migrations:migrate //run migrations
   ~~~~

### USAGE ###
1. check routes available
    ~~~~
    php bin/console debug:router 
   ~~~~

URI  | Method | Parameter
---------------------- | ---------------------- | ----------------------
/api/user/i-am-alive | GET |
/api/user/list | GET |
/api/user/create | POST | [username, email]
/api/user/update | POST | [username, email]
/api/user/delete |  POST | [username, email]
/api/user/{id} | GET |



2. Running tests
   1. ./vendor/bin/phpunit  
	
