<?php

namespace App\Tests\IAmAlive\Application;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class IAmAliveControllerTest extends WebTestCase
{

    /** @test */
    public function IAmAlive()
    {

        $client = $this->createclient();
        $client->request('GET', '/api/user/i-am-alive');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

    }

}