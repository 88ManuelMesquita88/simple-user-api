<?php


namespace App\Tests\User\Application;


use App\Tests\User\Infrastructure\Fixtures\UserFixture;
use App\User\Domain\Entity\User;
use App\User\Domain\Repository\UserRepositoryInterface;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class UserControllerTest extends WebTestCase
{
    use FixturesTrait;

    protected $serializer;

    public function test__construct()
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * @test
     */
    public function deleteUser(){

        $client = $this->createClient();
        $this->loadFixtures([UserFixture::class]);
        $client->request(
            'post',
            '/api/user/delete',
            ['email'=>'someEmail', 'username'=>'someUsername']
        );

        $this->assertEquals('200', $client->getResponse()->getStatusCode());

        $repository = $this->getRepository();
        $user = $repository->getUserByEmailOrUsername('someUsername');

        $this->assertTrue( is_null($user->getUsername()) );

    }

    /**
     * @test
     */
    public function updateUser(){

        $client = $this->createClient();

        $this->loadFixtures([UserFixture::class]);

        $client->request(
            'post',
            '/api/user/update',
            ['email'=>'someEmailUpdated', 'username'=>'someUsername']
        );

        $repository = $this->getRepository();
        $user = $repository->getUserByEmailOrUsername('someUsername');

        $this->assertEquals('200', $client->getResponse()->getStatusCode());
        $this->assertEquals('someEmailUpdated', $user->getEmail());

    }

    /**
     * @test
     */
    public function createUser(){

        $username = self::generateUsername();

        //TODO:: ensure email should have something like [A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}
        $email = "$username@someth1ng.sa";

        $client = $this->createClient();
        $client->request(
            'post',
            '/api/user/create',
            ['email'=>$email, 'username'=>$username]
        );

        $this->assertEquals('200', $client->getResponse()->getStatusCode());

        $repository = $this->getRepository();
        $user = $repository->getUserByEmailOrUsername($username);

        $this->assertFalse( is_null($user) );
        $this->assertEquals( $user->getUsername(), $username );


    }

    /**
     * @test
     */
    public function getUserEndpoint(){

        $id = 1;
        $client = $this->createClient();

        //make sure that at least 1 exists
        $this->loadFixtures([UserFixture::class]);
        $client->request(
            'get',
            '/api/user/' . $id
        );

        $this->assertEquals('200', $client->getResponse()->getStatusCode());

        $response  = json_decode($client->getResponse()->getContent(), true);
        $repository = $this->getRepository();
        $response_from_db = $repository->getUserById($id);

        $this->assertEquals($response_from_db->getUsername(), $response['username']);
        $this->assertEquals($response_from_db->getEmail(), $response['email']);

    }

    /**
     * @test
     */
    public function getUserListEndpoint(){

        $client = $this->createClient();
        $client->request(
            'get',
            '/api/user/list'
        );

        $this->assertEquals('200', $client->getResponse()->getStatusCode());

        $response  = json_decode($client->getResponse()->getContent(), true);

        $repository = $this->getRepository();
        $response_from_db = $repository->getUserList();

        $this->assertEquals( count($response_from_db), count($response) );

    }


    private function getRepository() : UserRepositoryInterface
    {
        /** @var UserRepositoryInterface $repository */
        $repository = self::bootKernel()->getContainer()->get("Test.App\User\Infrastructure\Repository\UserRepository");
        return $repository;
    }

    static function generateUsername()
    {
        return hash('sha256', 'thisIsATest' . uniqid() . microtime());
    }

}