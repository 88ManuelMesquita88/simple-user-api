<?php


namespace App\Tests\User\Infrastructure\Fixtures;


use App\User\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;

class UserFixture extends AbstractFixture implements ORMFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setEmail('someEmail');
        $user->setUsername('someUsername');

        $manager->persist($user);
        $manager->flush();
    }
}