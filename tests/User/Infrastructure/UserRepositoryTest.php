<?php


namespace App\Tests\User\Infrastructure;


use App\Tests\User\Infrastructure\Fixtures\UserFixture;
use App\User\Domain\Entity\User;
use App\User\Domain\Repository\UserRepositoryInterface;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;

class UserRepositoryTest extends WebTestCase
{

    use FixturesTrait;

    /**
     * @test
     */
    public function GetUserByUsername()
    {
        $this->loadFixtures([UserFixture::class]);
        $repository = $this->getRepository();
        $user = $repository->getUserByUsername('someUsername');
        $this->assertTrue($user instanceof User);
        $this->assertEquals('someUsername', $user->getUsername() );
        $this->assertEquals('someEmail', $user->getEmail() );
    }

    /**
     * @test
     */
    public function getUserByUsernameOrEmail()
    {
        $this->loadFixtures([UserFixture::class]);
        $repository = $this->getRepository();
        $user = $repository->getUserByEmailOrUsername('someUsername');
        $this->assertTrue($user instanceof User);
        $this->assertEquals('someEmail', $user->getEmail());
        $this->assertEquals('someUsername', $user->getUsername());
    }


    /**
     * @test
     */
    public function getUserListTest()
    {
        $this->loadFixtures([UserFixture::class]);
        $repository = $this->getRepository();
        $users = $repository->getUserList();
        $this->assertTrue( is_array($users) );
        $this->assertTrue( count($users) > 0 );

    }

    /**
     * @test
     */
    public function deleteUserTest(){

        $repository = $this->getRepository();

        $this->createUser();
        $list = $repository->getUserList();
        $user = end($list);


        $user = $repository->getUserByUsername($user->getUsername());
        $this->assertFalse( is_null($user->getId()) );

        $repository->delete($user);
        $this->assertTrue( is_null($user->getId()) );

    }


    /**
     * @test
     */
    public function createUser()
    {
        $repository = $this->getRepository();
        $user = $this->createNewUser();
        $repository->save($user);

        $user_from_db = $repository->getUserByEmailOrUsername($user->getUsername());
        $this->assertFalse( is_null($user_from_db));
    }

    /**
     * @test
     */
    public function UpdateUser(){

        $username = self::generateUsername();

        $this->loadFixtures([UserFixture::class]);
        $repository = $this->getRepository();
        $user = $repository->getUserByUsername('someUsername');

        $user->setUsername($username);
        $repository->save($user);

        $load_user_from_db = $repository->getUserById($user->getId());

        $this->assertTrue( isset($load_user_from_db) );
        $this->assertEquals( $username, $load_user_from_db-> getUsername());

    }



    private function getRepository() : UserRepositoryInterface
    {
        /** @var UserRepositoryInterface $repository */
        $repository = self::bootKernel()->getContainer()->get("Test.App\User\Infrastructure\Repository\UserRepository");
        return $repository;
    }

    private function createNewUser() : User
    {
        $username = self::generateUsername();
        $email = "$username@someth1ng.sm";

        $user = new User();
        $user->setUsername($username);
        $user->setEmail($email);
        return $user;
    }

    static function generateUsername()
    {
        return hash('sha256', 'thisIsATest' . uniqid() . microtime());
    }

}